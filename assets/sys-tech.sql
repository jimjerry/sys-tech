-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Sep 28, 2017 at 12:37 PM
-- Server version: 10.1.19-MariaDB
-- PHP Version: 7.0.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sys-tech`
--

-- --------------------------------------------------------

--
-- Table structure for table `deatails_info_person`
--

CREATE TABLE `deatails_info_person` (
  `id` int(11) NOT NULL,
  `buiding` varchar(11) DEFAULT NULL,
  `floor_name` varchar(11) DEFAULT NULL,
  `start_date` datetime DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `total_member` varchar(11) DEFAULT NULL,
  `member_details` varchar(300) DEFAULT NULL,
  `default_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `deatails_info_person`
--

INSERT INTO `deatails_info_person` (`id`, `buiding`, `floor_name`, `start_date`, `status`, `total_member`, `member_details`, `default_id`) VALUES
(7, '5', '2nd floor(', '0000-00-00 00:00:00', 1, '4', 'dawdawd\r\ntgrstgrstgrst', 23),
(8, '5', '2nd floor(', '0000-00-00 00:00:00', 0, '3', 'dADAD\r\nDAWAWDAW', 24),
(9, '5', '2nd floor(', '0000-00-00 00:00:00', 0, '3', 'dADAD\r\nDAWAWDAW', 25),
(10, '5', '2nd floor(', '0000-00-00 00:00:00', 1, '4', 'tyuki7yuj\r\ndryyhdryghr', 26),
(11, '5', '2nd floor(', '0000-00-00 00:00:00', 1, '7', 'gdfsdgs\r\ngsdegsdegs', 27),
(12, '5', '2nd floor(', '0000-00-00 00:00:00', 0, '4', 'hnfghgcgch', 28),
(13, '5', '2nd floor(', '0000-00-00 00:00:00', 0, '4', 'hnfghgcgch', 29),
(14, '5', '2nd floor(', '0000-00-00 00:00:00', 1, '7', 'ykjikjig\r\ntjutjyf', 30),
(15, '5', '2nd floor(', '0000-00-00 00:00:00', 1, '7', 'ykjikjig\r\ntjutjyf', 31),
(16, '5', '2nd floor(', '0000-00-00 00:00:00', 1, '7', 'ykjikjig\r\ntjutjyf', 32),
(17, '5', '2nd floor(', '0000-00-00 00:00:00', 1, '7', 'ykjikjig\r\ntjutjyf', 33),
(18, '5', '2nd floor(', '0000-00-00 00:00:00', 1, '7', 'ykjikjig\r\ntjutjyf', 34),
(19, '5', '0', '0000-00-00 00:00:00', 1, '7', 'bhjihvui', 35),
(20, '5', '0', '0000-00-00 00:00:00', 1, '4', 'qrwefr', 36),
(21, '5', 'test2', '0000-00-00 00:00:00', 0, '87', 'wdeasda\r\n', 37),
(22, '5', 'test2', '0000-00-00 00:00:00', 0, '87', 'wdeasda\r\n', 38),
(23, '5', 'test2', '0000-00-00 00:00:00', 1, '4', 'axxasxas\r\n', 39),
(24, '5', 'test3', '0000-00-00 00:00:00', 0, '7', 'daswda\r\ndadas', 40);

-- --------------------------------------------------------

--
-- Table structure for table `floor_info`
--

CREATE TABLE `floor_info` (
  `id` int(10) NOT NULL,
  `floor_name` varchar(10) DEFAULT NULL,
  `buidling_id` int(10) NOT NULL,
  `status` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `floor_info`
--

INSERT INTO `floor_info` (`id`, `floor_name`, `buidling_id`, `status`) VALUES
(21, 'test2', 5, '0'),
(22, 'test3', 5, '0');

-- --------------------------------------------------------

--
-- Table structure for table `login`
--

CREATE TABLE `login` (
  `id` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `type` varchar(10) NOT NULL,
  `full_name` varchar(100) NOT NULL,
  `email` varchar(50) NOT NULL,
  `phone` varchar(50) NOT NULL,
  `image` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `login`
--

INSERT INTO `login` (`id`, `username`, `password`, `type`, `full_name`, `email`, `phone`, `image`) VALUES
(23, 'jimjerry', '202cb962ac59075b964b07152d234b70', 'Owner', 'Donald Jerry Corraya', 'jimjerry00b@gmail.com', '01913171116', 'Capture_05.PNG'),
(39, 'donaldit', '202cb962ac59075b964b07152d234b70', 'Customer', 'ONEBANKLTD', 'example@gmail.com', '01913171116', 'Michael-jackson6.jpg'),
(40, 'lugjohlu', '202cb962ac59075b964b07152d234b70', 'Customer', 'ONEBANKLTD', 'example@gmail.com', '4854968498', 'Desert.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `payment_info`
--

CREATE TABLE `payment_info` (
  `id` int(11) NOT NULL,
  `advance_payment` varchar(20) DEFAULT NULL,
  `house_rent_per_month` varchar(20) DEFAULT NULL,
  `electricity_bill_per_month` varchar(20) DEFAULT NULL,
  `gass_bill_per_month` varchar(20) DEFAULT NULL,
  `water_bill_per_month` varchar(20) DEFAULT NULL,
  `other_payment` varchar(20) DEFAULT NULL,
  `default_id` int(11) DEFAULT NULL COMMENT 'user or admin or customers ID from login table'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `payment_info`
--

INSERT INTO `payment_info` (`id`, `advance_payment`, `house_rent_per_month`, `electricity_bill_per_month`, `gass_bill_per_month`, `water_bill_per_month`, `other_payment`, `default_id`) VALUES
(1, '15000', '12000', '850', '850', '600', '0', 23),
(2, '0', '0', '0', '0', '0', '0', 29),
(3, '56', '4564', '56456', '54665', '564', '564', 30),
(4, '56', '4564', '56456', '54665', '564', '564', 31),
(5, '56', '4564', '56456', '54665', '564', '564', 32),
(6, '56', '4564', '56456', '54665', '564', '564', 33),
(7, '56', '4564', '56456', '54665', '564', '564', 34),
(8, '00', '00', '00', '00', '00', '00', 35),
(9, '0', '0', '0', '0', '0', '0', 36),
(10, '0', '0', '0', '0', '0', '0', 37),
(11, '0', '0', '0', '0', '0', '0', 38),
(12, '0', '0', '0', '0', '0', '0', 39),
(13, '0', '0', '0', '0', '0', '0', 40);

-- --------------------------------------------------------

--
-- Table structure for table `property`
--

CREATE TABLE `property` (
  `id` int(10) NOT NULL,
  `property` varchar(50) DEFAULT NULL,
  `address` varchar(80) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `property`
--

INSERT INTO `property` (`id`, `property`, `address`) VALUES
(5, 'Shimana', 'South Kafrul');

-- --------------------------------------------------------

--
-- Table structure for table `transaction`
--

CREATE TABLE `transaction` (
  `id` int(11) UNSIGNED NOT NULL,
  `house_rent_per_month` varchar(20) DEFAULT NULL,
  `electricity_bill_per_month` varchar(20) DEFAULT NULL,
  `gass_bill_per_month` varchar(20) DEFAULT NULL,
  `water_bill_per_month` varchar(20) DEFAULT NULL,
  `other_payment` varchar(20) DEFAULT NULL,
  `total_amount` varchar(20) DEFAULT NULL,
  `default_id` int(11) DEFAULT NULL COMMENT 'user or admin or customers ID from login table',
  `date` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `transaction`
--

INSERT INTO `transaction` (`id`, `house_rent_per_month`, `electricity_bill_per_month`, `gass_bill_per_month`, `water_bill_per_month`, `other_payment`, `total_amount`, `default_id`, `date`) VALUES
(1, '12000', '1120', '850', '600', '0', '14570', 23, '2017-02-01'),
(2, '12000', '254', '850', '600', '0', '13704', 23, '2017-03-01'),
(3, '12000', '14', '850', '600', '0', '13464', 23, '2017-04-01'),
(4, '12000', '223', '850', '600', '0', '13673', 23, '2017-05-01'),
(5, '15000', '366', '850', '600', '654', '17470', 23, '2017-06-01'),
(6, '15000', '366', '850', '600', '654', '17470', 23, '2017-07-04'),
(7, '12000', '233', '850', '600', '0', '13683', 23, '2017-08-16'),
(8, '16000', '16365', '4564', '4564', '45', '41538', 39, '2017-09-27'),
(10, '18000', '900', '850', '600', '0', '20350', 40, '0000-00-00'),
(11, '12000', '955', '580', '656', '0', '14191', 40, '0000-00-00'),
(12, '12000', '750', '850', '600', '0', '14200', 23, '2017-09-12'),
(13, '12500', '850', '600', '650', '0', '14600', 39, '2017-09-28'),
(14, '9000', '564', '800', '712', '0', '11076', 40, '2017-09-25');

-- --------------------------------------------------------

--
-- Table structure for table `type_of_person`
--

CREATE TABLE `type_of_person` (
  `id` int(10) NOT NULL,
  `type` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `type_of_person`
--

INSERT INTO `type_of_person` (`id`, `type`) VALUES
(1, 'Admin'),
(2, 'Customer'),
(3, 'Owner');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `deatails_info_person`
--
ALTER TABLE `deatails_info_person`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `floor_info`
--
ALTER TABLE `floor_info`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `login`
--
ALTER TABLE `login`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payment_info`
--
ALTER TABLE `payment_info`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `property`
--
ALTER TABLE `property`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `transaction`
--
ALTER TABLE `transaction`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `type_of_person`
--
ALTER TABLE `type_of_person`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `deatails_info_person`
--
ALTER TABLE `deatails_info_person`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `floor_info`
--
ALTER TABLE `floor_info`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `login`
--
ALTER TABLE `login`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;
--
-- AUTO_INCREMENT for table `payment_info`
--
ALTER TABLE `payment_info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `property`
--
ALTER TABLE `property`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `transaction`
--
ALTER TABLE `transaction`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
