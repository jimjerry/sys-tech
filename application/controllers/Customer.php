<?php

    Class Customer extends CI_Controller{
        
        function __construct() {
            parent::__construct();        
            if (!$this->session->userdata("current_user_id")){
                redirect('Login');
            }
        }
        
        public function addNew(){
            $this->form_validation->set_rules('name', 'Name', 'required');
            /*$this->form_validation->set_rules('type', 'Type', 'required');
            $this->form_validation->set_rules('phone', 'Phone', 'required');
            $this->form_validation->set_rules('email', 'Email', 'required');
            $this->form_validation->set_rules('username', 'username', 'required');
            $this->form_validation->set_rules('password', 'password', 'required');
            $this->form_validation->set_rules('c_password', 'Confirm Password', 'required|matches[password]');
            //$this->form_validation->set_rules('national_id', 'national_id', 'required');
            $this->form_validation->set_rules('buiding', 'buiding', 'required');
            $this->form_validation->set_rules('floor_name', 'Floor Name', 'required');
            $this->form_validation->set_rules('status', 'status', 'required');
            $this->form_validation->set_rules('start_date', 'Start Date', 'required');
            $this->form_validation->set_rules('total_member', 'Total Member', 'required');
            $this->form_validation->set_rules('member_details', 'Member Details', 'required');
            $this->form_validation->set_rules('advance_payment', 'advance_payment', 'required');
            $this->form_validation->set_rules('advance_electricity_bill', 'advance_electricity_bill', 'required');
            $this->form_validation->set_rules('house_rent_per_month', 'house_rent_per_month', 'required');
            $this->form_validation->set_rules('electricity_bill_per_month', 'electricity_bill_per_month', 'required');
            $this->form_validation->set_rules('gass_bill_per_month', 'gass_bill_per_month', 'required');
            $this->form_validation->set_rules('water_bill_per_month', 'water_bill_per_month', 'required');
            $this->form_validation->set_rules('other_payment', 'other_payment', 'required');*/
         
            if($this->form_validation->run()) {
                
                $config['upload_path'] = './assets/images/users/';
                $config['allowed_types'] = 'gif|jpg|png|jpeg';
                $config['max_size'] = 10000;
//                $config['max_width'] = 1024;
//                $config['max_height'] = 768;
                $this->load->library('upload', $config);
                
                $name = $this->input->post('name');
                $type = $this->input->post('type');
                $phone = $this->input->post('phone');
                $email = $this->input->post('email');
                $username = $this->input->post('username');
                $c_password = md5($this->input->post('c_password'));
                //echo "test1 ".$name; exit();
                $attr = array(
                    'full_name' => $name,
                    'type' => $type,
                    'phone' => $phone,
                    'email' => $email,
                    'username' => $username,
                    'password' => $c_password,
                    'image' => $_FILES["image"]["name"],
                    'national_id' => $_FILES["national_id"]["name"],
                );
                
                if ($_FILES["image"]["name"]) {
                $this->upload->do_upload('image');
                $image_data = $this->upload->data();
                $insertData['image'] = $image_data['file_name'];
                }
               
                if ($_FILES["national_id"]["name"]) {
                $this->upload->do_upload('national_id');
                $nidImage = $this->upload->data();
                $insertData['national_id'] = $nidImage['file_name'];
                }
                
                $tableName = 'login';
                $user_id = $this->General_model->insertInfo2($tableName, $attr);
           
                
                $buiding = $this->input->post('buiding');
                $floor_name = $this->input->post('floor_name');
                $status = $this->input->post('status');
                $start_date = $this->input->post('start_date');
                $total_member = $this->input->post('total_member');
                $member_details = $this->input->post('member_details');
                
                $attr2 = array(
                    'buiding' => $buiding,
                    'floor_name' => $floor_name,
                    'start_date' => $start_date,
                    'status' => $status,
                    'total_member' => $total_member,
                    'member_details' => $member_details,
                    'default_id' => $user_id,
                    
                );
                
                $tableName2 = 'deatails_info_person';
                $result = $this->General_model->insertInfo($tableName2, $attr2);  
                // echo $this->db->last_query(); exit();
                   
                $advance_payment = $this->input->post('advance_payment');
                $advance_electricity_bill = $this->input->post('advance_electricity_bill');
                $house_rent_per_month = $this->input->post('house_rent_per_month');
                $electricity_bill_per_month = $this->input->post('electricity_bill_per_month');
                $gass_bill_per_month = $this->input->post('gass_bill_per_month');
                $water_bill_per_month = $this->input->post('water_bill_per_month');
                $other_payment = $this->input->post('other_payment');
                
                $attr3 = array(
                    'advance_payment' => $advance_payment,
                    'advance_electricity_bill' => $advance_electricity_bill,
                    'house_rent_per_month' => $house_rent_per_month,
                    'electricity_bill_per_month' => $electricity_bill_per_month,
                    'gass_bill_per_month' => $gass_bill_per_month,
                    'water_bill_per_month' => $water_bill_per_month,
                    'other_payment' => $other_payment,
                    'default_id' => $user_id,
                    
                );
                
                $tableName3 = 'payment_info';
                $result = $this->General_model->insertInfo($tableName3, $attr3); 
                
                $attr4 = array(
                    'status' => $status,
                );
                $tableName4 = 'floor_info';
                $whr4 = '';
                $result = $this->General_model->updateInfo($tableName4, $attr4, $whr4="");
                //echo $this->db->last_query(); exit;
                
                
                
            } 
            
            
            // echo validation_errors(); exit;
            
            $table = 'type_of_person';
            $table2 = 'property';
            $table3 = 'floor_info';
            $select = 'type';
            $select2 = 'property,address,id';
            $select3 = 'floor_name, buidling_id, `status`';
            $whr3 = 'status`= 0';
            $groupBy = 'buidling_id';
            $data['typeOfperson'] = $this->General_model->infoQuery($table, $select);
            $data['typeOfperson2'] = $this->General_model->infoQuery($table2, $select2);
            $data['typeOfperson3'] = $this->General_model->infoQuery($table3, $select3, $join="", $groupBy="", $whr3);
            $data['addNew'] = 'dashboard/addNew_page';
            $this->load->view('dashboard_layout', $data);
        }
        
        public function myBill(){
            $data['myBill'] = 'dashboard/myBill';
            $this->load->view('dashboard_layout', $data);
        }
        
        public function ViewUserHistory(){
            
            $id = $this->input->get('id');
            //exit();
            
            $this->form_validation->set_rules('houseRent', 'House Rent', 'required');
            $this->form_validation->set_rules('electricBill', 'Electric Bill', 'required');
            $this->form_validation->set_rules('gassBill', 'Gass Bill', 'required');
            $this->form_validation->set_rules('waterBill', 'Water Bill', 'required');
            $this->form_validation->set_rules('otherBill', 'Other Bill', 'required');
            $this->form_validation->set_rules('totalAmount', 'Total Amount', 'required');    
            $this->form_validation->set_rules('start_date', 'Total Amount', 'required');    
            
            if($this->form_validation->run()){
                
                $houseRent = $this->input->post('houseRent');
                $electricBill = $this->input->post('electricBill');
                $gassBill = $this->input->post('gassBill');
                $waterBill = $this->input->post('waterBill');
                $otherBill = $this->input->post('otherBill');
                $totalAmount = $this->input->post('totalAmount');
                $default_id = $id;
                $date = $this->input->post('start_date');
                
                $attr = array(
                    'house_rent_per_month' => $houseRent,
                    'electricity_bill_per_month' => $electricBill,
                    'gass_bill_per_month' => $gassBill,
                    'water_bill_per_month' => $waterBill,
                    'other_payment' => $otherBill,
                    'total_amount' => $totalAmount,
                    'default_id' => $default_id,
                    'date' => $date,
                );
                $table= 'transaction';
                $result = $this->General_model->insertInfo($table, $attr);
                
                //echo $this->db->last_query();
                //exit();
                
            }
            
            
            $select='login.id, login.full_name, login.phone, login.national_id, login.image, login.type,payment_info.advance_electricity_bill,payment_info.advance_payment, payment_info.advance_payment, payment_info.house_rent_per_month, payment_info.electricity_bill_per_month, payment_info.gass_bill_per_month, payment_info.water_bill_per_month';
            $from='login';
            $join = array('payment_info','login.id = payment_info.default_id','INNER');
            $whr = array(
                'login.id' => $id
            );
            
            $whr2 = array(
                'default_id' => $id
            );
            $select2 = '*';
            $from2='transaction';   
            
            $data['ViewUserHistoryData'] = $this->General_model->SingleRowQuery($from, $select, $whr, $join, $groupBy="" );
            //echo $this->db->last_query(); exit;
            $data['ViewUserHistoryData2'] = $this->General_model->infoQuery($from2, $select2, $join="" ,$groupBy="", $whr2);
            
                    
            //$month = date("F");
            //$year = date("Y");
            //$numberOfMonth = date('m', strtotime("$month"));
            $dataOfmoth =date('Y-m-d');//'2017-07-01' ;//$year."-".$numberOfMonth."-01";
                       
                    
            $data['monthData']=$this->General_model->getStatusofCustomer($dataOfmoth,$id);
         
            $data['ViewUserHistory_page'] = 'dashboard/ViewUserHistory_page';
            $this->load->view('dashboard_layout', $data);
        }
        
        public function editInfo(){
            $id = $this->input->get('id');
            
            $this->form_validation->set_rules('name', 'Name', 'required');
            
            if($this->form_validation->run()) {
                $config['upload_path'] = './assets/images/users/';
                $config['allowed_types'] = 'gif|jpg|png|jpeg';
                $config['max_size'] = 10000;
                
                $this->load->library('upload', $config);
                
                $name = $this->input->post('name');
                $type = $this->input->post('type');
                $phone = $this->input->post('phone');
                $email = $this->input->post('email');
                $username = $this->input->post('username');
                $c_password = md5($this->input->post('c_password'));
                $attr = array(
                    'full_name' => $name,
                    'type' => $type,
                    'phone' => $phone,
                    'email' => $email,
                    'username' => $username,
                    'image' => $_FILES["image"]["name"],
                    'national_id' => $_FILES["national_id"]["name"],
                );
                
                
                if ($_FILES["image"]["name"]) {
                $this->upload->do_upload('image');
                $image_data = $this->upload->data();
                $insertData['image'] = $image_data['file_name'];
                }
               
                if ($_FILES["national_id"]["name"]) {
                $this->upload->do_upload('national_id');
                $nidImage = $this->upload->data();
                $insertData['national_id'] = $nidImage['file_name'];
                }
                $whr = array(
                    'login.id' => $id
                );
                $tableName = 'login';
                $user_id = $this->General_model->updateInfo2($tableName, $attr, $whr);  
                //echo $this->db->last_query(); exit();
                
                
                $buiding = $this->input->post('buiding');
                $floor_name = $this->input->post('floor_name');
                $status = $this->input->post('status');
                $start_date = $this->input->post('start_date');
                $total_member = $this->input->post('total_member');
                $member_details = $this->input->post('member_details');
                
                $attr2 = array(
                    'buiding' => $buiding,
                    'floor_name' => $floor_name,
                    'start_date' => $start_date,
                    'status' => $status,
                    'total_member' => $total_member,
                    'member_details' => $member_details,                    
                );
                $whrID = array(
                    'deatails_info_person.default_id' => $id,
                );
                $tableName2 = 'deatails_info_person';
                $result = $this->General_model->updateInfo($tableName2, $attr2, $whrID);  
                //echo $this->db->last_query(); exit();
                
                $tableName3 = 'payment_info';
                
                $advance_payment =  $this->input->post('advance_payment');
                $advance_electricity_bill   =   $this->input->post('advance_electricity_bill');
                $house_rent_per_month   =   $this->input->post('house_rent_per_month');
                $electricity_bill_per_month =   $this->input->post('electricity_bill_per_month');
                $gass_bill_per_month    =   $this->input->post('gass_bill_per_month');
                $water_bill_per_month   =   $this->input->post('water_bill_per_month');
                $other_payment  =   $this->input->post('other_payment');
                
                $attr3  = array(
                    'advance_payment' => $advance_payment,
                    'advance_electricity_bill' => $advance_electricity_bill,
                    'house_rent_per_month' => $house_rent_per_month,
                    'electricity_bill_per_month' => $electricity_bill_per_month,
                    'gass_bill_per_month' => $gass_bill_per_month,
                    'water_bill_per_month' => $water_bill_per_month,
                    'other_payment' => $other_payment,
                    // 'default_id' => $user_id,
                );
                $whrID3 = array(
                    'default_id' => $id,
                );
                $result = $this->General_model->updateInfo($tableName3, $attr3, $whrID3);  
            }

            $selectBuding = "*";
            $tableBuding = 'property';            
            $data['budingInformation'] = $this->General_model->infoQuery($tableBuding, $selectBuding, $join="", $groupBy="", $whr="");  
            $whr = "login.id = payment_info.default_id AND payment_info.default_id = login.id AND deatails_info_person.default_id = login.id AND login.id = $id";
            $select= 'login.username, login.`password`, login.type, login.full_name, login.email, login.phone, login.image, login.national_id, payment_info.advance_payment, payment_info.advance_electricity_bill, payment_info.house_rent_per_month, payment_info.electricity_bill_per_month, payment_info.gass_bill_per_month, payment_info.water_bill_per_month, payment_info.other_payment,deatails_info_person.buiding, deatails_info_person.floor_name, deatails_info_person.start_date, deatails_info_person.start_date, deatails_info_person.total_member, deatails_info_person.member_details, deatails_info_person.status';
            $from = 'login, deatails_info_person, payment_info';
            
            $data['editInfoData'] =$this->General_model->SingleRowQuery($from, $select, $whr, $join ="", $groupBy="" );
            //echo $this->db->last_query(); exit();
            //$data['floorInforMation'] = $this->General_model->infoQuery('floor_info', 'floor_name, buidling_id, `status`', $join="", $groupBy="", 'status`= 0');
            
            $data['editInfor'] = 'dashboard/edit_page';
            $this->load->view('dashboard_layout', $data);
        }
        
    }

?>