<?php

Class Home extends CI_Controller {

    function __construct() {
        parent::__construct();
    }
    
    public function index(){
        /* SELECT * FROM logo_text WHERE `status`=1 */
        $table2 = 'logo_text';
        $star = '*';
        $statusID = '1';
        $orderBy= 'id';
        $whr = array(
            'status' => $statusID
        );
        
        $checkLogo = $this->General_model->checkRowQuery($table2, $star, $whr, $orderBy);
        if($checkLogo){
            $data['name'] = $checkLogo->logo_name;
        } else{
            $data['name'] = "Business";
        }
        
        $data['site_home'] = 'site/home';
        $this->load->view('home_page_layout', $data);
    }

}

?>