<?php

Class Services extends CI_Controller {

    function __construct() {
        parent::__construct();
    }

    public function index() {
        
        $table2 = 'logo_text';
        $star = '*';
        $statusID = '1';
        $orderBy= 'id';
        $whr = array(
            'status' => $statusID
        );
        
        $checkLogo = $this->General_model->checkRowQuery($table2, $star, $whr, $orderBy);
        if($checkLogo){
            $data['name'] = $checkLogo->logo_name;
        } else{
            $data['name'] = "Business";
        }
        $data['services'] = 'site/services';
        $this->load->view('home_page_layout', $data);
    }

}

?>
