<?php

Class Owner extends CI_Controller {

    function __construct() {
        parent::__construct();
        if (!$this->session->userdata("current_user_id")) {
            redirect('Login');
        }
    }

    public function index() {
        redirect('Owner/property');
    }

    public function property() {
        $table = 'floor_info';
        $groupBy = 'buidling_id';
        $select = 'floor_info.id,floor_info.`status`, floor_info.floor_name, property.property, floor_info.buidling_id,property.address, COUNT(floor_info.floor_name) as total_floor, (SUM(CASE WHEN floor_info.status > 0 THEN 1 ELSE 0 END)) as total_active';
        $join = array('property', 'floor_info.buidling_id = property.id', 'left');
        $whr = "";
        $data['PropertData'] = $this->General_model->infoQuery($table, $select, $join, $groupBy, $whr);
        //echo "<pre>"; print_r($PropertData); exit();
        //echo $this->db->last_query(); exit();
        $data['property'] = 'dashboard/property';
        $this->load->view('dashboard_layout', $data);
    }

    public function addNewProperty() {

        $this->form_validation->set_rules('propertyName', 'Property Name', 'required');
        $this->form_validation->set_rules('addressOfProperty', 'Address Property', 'required');

        if ($this->form_validation->run()) {

            $PropertyName = $this->input->post('propertyName');
            $AddreOfProperty = $this->input->post('addressOfProperty');

            $attr = array(
                'property' => $PropertyName,
                'address' => $AddreOfProperty
            );

            $table = 'property';
            $propertyData = $this->General_model->insertInfo($table, $attr);

            if ($propertyData) {
                redirect('Owner/addNewFloor');
            } else {
                redirect('Owner/addNewProperty');
            }
        }

        $data['addNewProperty'] = 'dashboard/addNewProperty';
        $this->load->view('dashboard_layout', $data);
    }

    public function addNewFloor() {

        $this->form_validation->set_rules('floorname', 'Floor Name', 'required');
        $this->form_validation->set_rules('budiling', 'Budiling', 'required');

        if ($this->form_validation->run()) {

            $floor_name = $this->input->post('floorname');
            $buidling_id = $this->input->post('budiling');
            $status = $this->input->post('status');

            $attr = array(
                'floor_name' => $floor_name,
                'buidling_id' => $buidling_id,
                'status' => $status,
            );

            $table = 'floor_info';
            $propertyData = $this->General_model->insertInfo($table, $attr);
            if ($propertyData) {
                redirect('Owner/addNewFloor');
            } else {
                redirect('Owner/addNewProperty');
            }
        }

        $table = 'property';
        $groupBy = 'property';
        $select = 'property.property, property.id';
        $whr = "";
        $data['PropertData'] = $this->General_model->infoQuery($table, $select, $join = "", $groupBy, $whr);
        $data['addNewFloor'] = 'dashboard/addNewFloor';
        $this->load->view('dashboard_layout', $data);
    }

    public function propertyView() {
        $id = $this->input->get('id');
        $whr = array(
            'buidling_id' => $id
        );
        $whr2 = array(
            'id' => $id
        );
         
        $select = 'login.id, login.full_name, login.phone, login.email, login.image, deatails_info_person.floor_name, deatails_info_person.`status`, deatails_info_person.buiding';
        $join = array(
             array( 'deatails_info_person', 'login.id=deatails_info_person.default_id', 'INNER'),
             //array('property pro', 'dip.buiding=pro.id', 'INNER'), 
        ); 
      
        $whr=array(
            'deatails_info_person.building_id'=>$id,
            //'deatails_info_person.status' => 1
        );
 

        $data['PropertyViewData'] = $this->General_model->infoQueryLarge('login', $select, $join, $groupBy="", $whr);
       //echo $this->db->last_query(); exit();
        $data['PropertyViewData2'] = $this->General_model->checkRowQuery('property', '*', $whr2);

        $dataOfmoth = date('Y-m-d');
        $monthData = $this->General_model->getStatusofAllCustomer($dataOfmoth);
        //echo $this->db->last_query(); exit();
        $paidlist = array();
        if ($monthData) {
            foreach ($monthData as $key) {
                array_push($paidlist, $key->default_id);
            }
        }
        $data['paidArray'] = $paidlist;

        $data['PropertyView_Page'] = 'dashboard/PropertyView_Page';
        $this->load->view('dashboard_layout', $data);
    }

    public function payableBill() {
        $id = $this->input->get('id');
        $house_rent_per_month = $this->input->post('house_rent_per_month');
        $electricity_bill_per_month = $this->input->post('electricity_bill_per_month');
        $gass_bill_per_month = $this->input->post('gass_bill_per_month');
        $water_bill_per_month = $this->input->post('water_bill_per_month');

        $whr = array(
            'default_id' => $id
        );
        $attr = array(
            'house_rent_per_month' => $house_rent_per_month,
            'electricity_bill_per_month' => $electricity_bill_per_month,
            'gass_bill_per_month' => $gass_bill_per_month,
            'water_bill_per_month' => $water_bill_per_month,
        );


        $table = 'payment_info';
        $result = $this->General_model->updateInfo($table, $attr, $whr);
        //echo $this->db->last_query();
        //echo $id;
        //exit();
        if ($result) {
            redirect('Customer/ViewUserHistory?id=' . $id);
        } else {
            echo "not ok";
        }
    }

}

?>