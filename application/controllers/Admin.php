<?php

Class Admin extends CI_Controller {
    
    function __construct() {
        parent::__construct();
    }
    
    public function index(){
        $table = 'login';
        $select= '*';
        
        $data['allUsers'] = $this->General_model->infoQuery($table, $select, $join=0, $groupBy=0, $whr=0);        
        $data['admin_page'] = 'dashboard/admin_page';
        $this->load->view('dashboard_layout', $data);
    }
    
    
    
    public function urlRedirect($lang){
      $this->session->set_userdata('lang', $lang);
      redirect("Dashboard");
    }
}