<?php

Class Customising extends CI_Controller {

    function __construct() {
        parent::__construct();
        if (!$this->session->userdata("current_user_id")) {
            redirect('Login');
        }
        if (strlen($this->session->userdata("lang")) == 2) {
            $this->lang->load('dashboard', $this->session->userdata("lang"));
        } else {
            $this->lang->load('dashboard', 'en');
        }
    }

    public function LogoChange() {

        $this->form_validation->set_rules('logoText', 'Logo Text', 'required');
        $this->form_validation->set_rules('status', 'Status', 'required');

        if ($this->form_validation->run()) {
            $logoText = $this->input->post('logoText');
            $status = $this->input->post('status');

            $attr = array(
                'logo_name' => $logoText,
                'status' => $status
            );
            $table = 'logo_text';
            $logoTextInsert = $this->General_model->insertInfo($table, $attr);
        }
        $table2 = 'logo_text';
        $star = '*';
        $statusID = '1';
        $orderBy = 'id';
        $whr = array(
            'status' => $statusID
        );
        $checkLogo = $this->General_model->checkRowQuery($table2, $star, $whr, $orderBy);
//        echo $this->db->last_query();
//        //var_dump($checkLogo);
//        exit();

        if ($checkLogo) {
            $data['name'] = $checkLogo->logo_name;
        } else {
            $data['name'] = "";
        }

        $data['LogoChange'] = 'LogoChange';
        $this->load->view('dashboard_layout', $data);
    }

    public function ResetLogoChange() {
        /* DELETE FROM logo_text */
        $table = 'logo_text';

        $attr = 'status';

        $resetLogo = $this->General_model->updateResetStatus($table, $attr);
        if ($resetLogo) {
            redirect('Customising/LogoChange');
        }
    }

}

?>