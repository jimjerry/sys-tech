<?php

Class Login extends CI_Controller {

    function __construct() {
        parent::__construct();   
        
        
    }


    public function index() {        
         
        
        if ($this->session->userdata("current_user_id")){
         
            redirect('Dashboard');
        }
        
        $this->form_validation->set_rules('username', 'Username', 'required');
        $this->form_validation->set_rules('password', 'Password', 'required');
        $this->form_validation->set_rules('type', 'Type', 'required');

        if ($this->form_validation->run()) {

            $username = $this->input->post('username');
            $password = md5($this->input->post('password'));
            $type = $this->input->post('type');

            $attr = array(
                'username' => $username,
                'password' => $password,
                'type' => $type
            );

            $table = 'login';
            $select = '*';
            $result = $this->General_model->checkRowQuery($table, $select, $attr);
             //echo $this->db->last_query($result); exit();
            if ($result) {

                $userData = array(
                    'current_user_id' => $result->id,
                    'current_username' => $result->username,
                    'current_password' => $result->password,
                    'current_type' => $result->type,
                    'current_user_fullName' => $result->full_name,
                    'current_user_image' => $result->image,
                );                
                $this->session->set_userdata($userData);
                redirect('Dashboard');
            } else {
                $this->load->view('dashboard/login_page');
            }
        }

        $this->load->view('dashboard/login_page');
    }
    
    public function editInfo(){
        $id = $this->input->get('id');
        $select = '*';
        $table = 'property';
        $whr = array(
            'id' => $id
        );
        
        $result = $this->General_model->checkRowQuery($table, $select, $whr);
        // echo $this->db->last_query($result); exit();
    }

    public function logout() {
        
        $this->session->unset_userdata('current_user_id');
        $this->session->unset_userdata('current_username');
        $this->session->unset_userdata('current_password');
        $this->session->unset_userdata('current_type');
        
        $this->session->sess_destroy();
        redirect('Login');
    }
    
    public function delete(){
        
        $deleteId = $this->input->post('delid');
        $table = array(
            'login' => 'id',
            'deatails_info_person' => 'default_id',
            'payment_info' => 'default_id',
           // 'transaction' => 'default_id',
        );
        $error_array=array();
        foreach($table as $index=>$key)
        {
            $result=$this->General_model->delete($index, array($key => $deleteId));            
            
            if(!$result)
            {
                
                
                $missintTable="table_name:".$index."column_name:".$key."valueof_id:".$deleteId;
               array_push($error_array,$missintTable);

            }
        }
         
        if(empty($error_array))
        {
            echo "done";
            
        }
        else{
           // print_r($error_array);exit;
            return $error_array;
            
        }
    }

}

?>