<?php

Class Dashboard extends CI_Controller{
    
   function __construct() {
        parent::__construct();
                
        if (!$this->session->userdata("current_user_id")){
            redirect('Login');
        }
        if( strlen($this->session->userdata("lang"))==2){
            $this->lang->load('dashboard', $this->session->userdata("lang"));
        }else{
            $this->lang->load('dashboard', 'en');
        }
        
    }
    
    public function index(){        
        $data['home'] = 'home';
        $this->load->view('dashboard_layout', $data);
    }
    
    public function typeOfperson(){
        $select = 'type_of_person';
        $table = 'type';
        $typeOfperson = $this->General_model->infoQuery($select, $table);
    }
}

?>