<?php

Class General_model extends CI_Model {

    public function checkRowQuery($from, $table, $attr, $orderBy=0) {
        $this->db->from($from);
        $this->db->order_by($orderBy, "desc");
        $this->db->select($table);
        if ($attr) {
            $this->db->where($attr);
        }

        $query = $this->db->get();
        if ($query) {
            return $query->row();
        } else {
            return FALSE;
        }
    }

    public function infoQuery($select, $table, $join = "", $groupBy = "", $whr = "") {
        $this->db->select($table);
        $this->db->from($select);
        if ($join) {
            $this->db->join($join[0], $join[1], $join[2]);
        }
        if ($groupBy) {
            $this->db->group_by($groupBy);
        }
        if ($whr) {
            $this->db->where($whr);
        }
        $data = $this->db->get();
        if ($data) {
            return $data->result();
        } else {
            return FALSE;
        }
    }

    public function infoQueryLarge($select, $table, $join = "", $groupBy = "", $whr = "") {
        $this->db->select($table);
        $this->db->from($select);
        if ($join) {
            foreach ($join as $key) {
                $this->db->join($key[0], $key[1], $key[2]);
            }
        }
        if ($groupBy) {
            $this->db->group_by($groupBy);
        }
        if ($whr) {
            $this->db->where($whr);
        }
        $data = $this->db->get();
        if ($data) {
            return $data->result();
        } else {
            return FALSE;
        }
    }

    public function insertInfo($table, $attr) {
        $this->db->insert($table, $attr);
        $afftectedRows = $this->db->affected_rows();

        if ($afftectedRows) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function insertInfo2($table, $attr) {
        $this->db->insert($table, $attr);
        $insert_id = $this->db->insert_id();
        if ($insert_id) {
            return $insert_id;
            ;
        } else {
            return FALSE;
        }
    }

    public function updateInfo2($table, $attr, $whr) {
        if ($whr) {
            $this->db->where($whr);
        }
        $this->db->update($table, $attr);
        $result = $this->db->insert_id();
        if ($result) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function SingleRowQuery($table, $select, $whr, $join, $groupBy) {

        $this->db->from($table);
        $this->db->select($select);
        $this->db->where($whr);
        if ($join) {
            $this->db->join($join[0], $join[1], $join[2]);
        }
        if ($groupBy) {
            $this->db->group_by($groupBy);
        }

        $query = $this->db->get();
        if ($query) {
            return $query->row();
        } else {
            return FALSE;
        }
    }

    public function updateInfo($table, $attr, $whr) {

        if ($whr) {
            $this->db->where($whr);
        }
        $result = $this->db->update($table, $attr);
        if ($result) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
    public function updateResetStatus($table, $attr) {
        $this->db->set($attr, '0');
        $result = $this->db->update($table);
        if ($result) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function getStatusofCustomer($date, $id) {
        $sql = "SELECT default_id,date from `transaction` where DATE_FORMAT(date,'%Y-%m') = DATE_FORMAT('" . $date . "','%Y-%m') and default_id='" . $id . "'";
        $query = $this->db->query($sql);

        if ($query) {
            return $query->row();
        } else {
            return FALSE;
        }
    }

    function getStatusofAllCustomer($date) {
        $sql = "SELECT default_id,date from `transaction` where DATE_FORMAT(date,'%Y-%m') = DATE_FORMAT('" . $date . "','%Y-%m')";
        $query = $this->db->query($sql);

        if ($query) {
            return $query->result();
        } else {
            return FALSE;
        }
    }

    public function delete($table, $whr) {
        $this->db->delete($table, $whr);

        if ($this->db->affected_rows() == 1) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function deleteData($table, $star, $whr) {
        $this->db->delete($table, $star, $whr);
        
        if ($this->db->affected_rows() == 1) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
 

}

?>