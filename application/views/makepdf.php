<?php
//============================================================+
// File name   : example_001.php
// Begin       : 2008-03-04
// Last Update : 2013-05-14
//
// Description : Example 001 for TCPDF class
//               Default Header and Footer
//
// Author: Nicola Asuni
//
// (c) Copyright:
//               Nicola Asuni
//               Tecnick.com LTD
//               www.tecnick.com
//               info@tecnick.com
//============================================================+

/**
 * Creates an example PDF TEST document using TCPDF
 * @package com.tecnick.tcpdf
 * @abstract TCPDF - Example: Default Header and Footer
 * @author Nicola Asuni
 * @since 2008-03-04
 */

// Include the main TCPDF library (search for installation path).
//require_once('tcpdf_include.php');

// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Nicola Asuni');
$pdf->SetTitle('TCPDF Example 001');
$pdf->SetSubject('TCPDF Tutorial');
$pdf->SetKeywords('TCPDF, PDF, example, test, guide');

// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 001', PDF_HEADER_STRING, array(0,64,255), array(0,64,128));
$pdf->setFooterData(array(0,64,0), array(0,64,128));

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
	require_once(dirname(__FILE__).'/lang/eng.php');
	$pdf->setLanguageArray($l);
}

// ---------------------------------------------------------

// set default font subsetting mode
$pdf->setFontSubsetting(true);

// Set font
// dejavusans is a UTF-8 Unicode font, if you only need to
// print standard ASCII chars, you can use core fonts like
// helvetica or times to reduce file size.
$pdf->SetFont('dejavusans', '', 14, '', true);

// Add a page
// This method has several options, check the source code documentation for more information.
$pdf->AddPage();

// set text shadow effect
$pdf->setTextShadow(array('enabled'=>true, 'depth_w'=>0.2, 'depth_h'=>0.2, 'color'=>array(196,196,196), 'opacity'=>1, 'blend_mode'=>'Normal'));

// Set some content to print
$html = <<<EOD
       <!DOCTYPE html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">
        <style type="text/css">
            body{
                font-family: arial;
                background: black;
            }
            a{

            }
            p{
                font-size: 15px;
                line-height: 22px;
                padding: 0;
                margin: 0;
            }
            b{
                font-size: 15px;
                line-height: 31px;
                padding: 0;
                margin: 0;
            }

            h1{
                font-size: 20px;
                padding: 5px;
                margin: 0;
            }
            h2{
                font-size: 16px;
                padding: 5px;
                margin: 0;
            }
            h3{

            }
            h4{

            }

            .mainDIV{
                background: #fff; margin: 0 auto;
            }
        </style>
    </head>
    <body>
        <table width="100%">
            <tr>
                <td>
                    <table width="580" height="auto" class="mainDIV">
                        <tr>
                            <td>
                                <!--Start-->

                                <table width="100%" height="100" style="border-bottom: 1px solid #000;">
                                    <tr>
                                        <td width="580">
                                            <table width="100%">
                                                <tr>
                                                    <td align="center">
                                                        <h1>House Name</h1>
                                                        <p>300/1 South Kafrul,Dhaka-Cantonment,Dhaka-1206,<br>Ph: 4648449874987, 8798798798</p>
                                                        <b>Alamgir Hossain</b>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>  
                                <!--Main-->
                                <table width="100%">
                                    <tr>
                                        <td width="20"></td>
                                        <td width="540" height="500" valign="top">
                                            <table width="100%" align="center" border="1">
                                                <tr>
                                                    <th>Details</th>
                                                    <th>Taka</th>
                                                </tr>

                                                <tr>
                                                    <td>House Rent</td>
                                                    <td>10000</td>
                                                </tr>
                                                <tr>
                                                    <td>Electricity Bill</td>
                                                    <td>1400</td>
                                                </tr>
                                                <tr>
                                                    <td>Gass Bill</td>
                                                    <td>800</td>
                                                </tr>
                                                <tr>
                                                    <td>Water Bill</td>
                                                    <td>700</td>
                                                </tr>
                                                <tr>
                                                    <td>Others</td>
                                                    <td>0.00</td>
                                                </tr>
                                                <tr>
                                                    <th>Total</th>
                                                    <th>12,355</th>
                                                </tr>
                                            </table>
                                        </td>
                                        <td width="20"></td>
                                    </tr>                                    
                                </table>


                                <table width="100%">
                                    <tr>
                                        <td width="20"></td>
                                        <td width="540">
                                            <table>
                                                <tr>
                                                    <td width="265" align="center" style="border-top: 1px solid #000;">
                                                        Sign of Owner
                                                    </td>
                                                    <td width="10"></td>
                                                    <td width="265" align="center" style="border-top: 1px solid #000;">
                                                        Sign of Renter
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td width="20"></td>
                                        
                                    </tr>
                                </table>
                                <!--Main End-->
                                <table width="100%" height="100" style="">
                                    <tr>
                                        <td></td>
                                    </tr>
                                </table>
                                <!--END-->
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </body>
</html>
EOD;

// Print text using writeHTMLCell()
$pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);

// ---------------------------------------------------------

// Close and output PDF document
// This method has several options, check the source code documentation for more information.
$pdf->Output('example_001.pdf', 'I');

//============================================================+
// END OF FILE
//============================================================+
