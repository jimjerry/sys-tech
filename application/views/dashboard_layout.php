<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>SYS</title>
        <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/css/main.css">
        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"> 
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css"> 
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.4.2/css/buttons.dataTables.min.css"> 
        <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/css/style.css"> 
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>

        <style type="text/css">
            .ui-state-highlight, .ui-widget-content .ui-state-highlight, .ui-widget-header, .ui-state-highlight {
                background: #009688;
                //border: 1px solid #555;
                color: #fff;
            }
            .ui-datepicker-year, .ui-datepicker-month{
                background: transparent;
            }
            .ui-datepicker-month option{
                background: #009688;
            }
            .ui-datepicker-month option selectd{
                background: #fff!important;
                color: #000!important;
            }
        </style>
    </head>
    <body class="sidebar-mini fixed">
        <div class="wrapper">
            <!-- Navbar-->
            <header class="main-header hidden-print"><a class="logo" href="<?= base_url('Dashboard'); ?>"><?=$this->lang->line('site_logo');?></a>
                <nav class="navbar navbar-static-top">
                    <!-- Sidebar toggle button--><a class="sidebar-toggle" href="#" data-toggle="offcanvas"></a>
                    <!-- Navbar Right Menu-->
                    <div class="navbar-custom-menu">
                        <ul class="top-nav">
                            <!--Notification Menu-->
                            <li class="dropdown notification-menu">
                                <a class="dropdown-toggle" href="#" data-toggle="dropdown" aria-expanded="false">
                                    <i class="fa fa-bell-o fa-lg"></i>
                                </a>
                                <ul class="dropdown-menu">
                                    <li class="not-head">You have 4 new notifications.</li>
                                    <li><a class="media" href="javascript:;"><span class="media-left media-icon"><span class="fa-stack fa-lg"><i class="fa fa-circle fa-stack-2x text-primary"></i><i class="fa fa-envelope fa-stack-1x fa-inverse"></i></span></span>
                                            <div class="media-body"><span class="block">Lisa sent you a mail</span><span class="text-muted block">2min ago</span></div></a></li>
                                    <li><a class="media" href="javascript:;"><span class="media-left media-icon"><span class="fa-stack fa-lg"><i class="fa fa-circle fa-stack-2x text-danger"></i><i class="fa fa-hdd-o fa-stack-1x fa-inverse"></i></span></span>
                                            <div class="media-body"><span class="block">Server Not Working</span><span class="text-muted block">2min ago</span></div></a></li>
                                    <li><a class="media" href="javascript:;"><span class="media-left media-icon"><span class="fa-stack fa-lg"><i class="fa fa-circle fa-stack-2x text-success"></i><i class="fa fa-money fa-stack-1x fa-inverse"></i></span></span>
                                            <div class="media-body"><span class="block">Transaction xyz complete</span><span class="text-muted block">2min ago</span></div></a></li>
                                    <li class="not-footer"><a href="#">See all notifications.</a></li>
                                </ul>
                            </li>
                            <!-- User Menu-->
                            <li class="dropdown"><a class="dropdown-toggle" href="#" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-user fa-lg"></i></a>
                                <ul class="dropdown-menu settings-menu">
                                    <li><a href="page-user.html"><i class="fa fa-cog fa-lg"></i> Chance Password</a></li>
                                    <li><a href="page-user.html"><i class="fa fa-user fa-lg"></i> Profile</a></li>
                                    <li><a href="<?= base_url(); ?>Login/logout"><i class="fa fa-sign-out fa-lg"></i> Logout</a></li>
                                </ul>
                            </li>
                            <li class="dropdown"><a class="dropdown-toggle" href="#" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                    <img src="
                                         <?php
                                            if($this->session->userdata("lang")== 'en'){
                                                echo site_url()."assets/images/flags/eng.jpg";
                                            }elseif($this->session->userdata("lang")== 'bd') {
                                                echo site_url()."assets/images/flags/ban.jpg";
                                            }elseif($this->session->userdata("lang")== 'in') {
                                                echo site_url()."assets/images/flags/ind.jpg";
                                            }else{
                                                echo site_url()."assets/images/flags/eng.jpg";
                                            }
                                         ?>
                                         "></a>
                                <ul class="dropdown-menu settings-menu">
                                    <li><a href="<?= site_url('admin/urlRedirect')?>/en"><img src="<?= base_url()?>assets/images/flags/eng.jpg"> English</a></li>
                                    <li><a href="<?= site_url('admin/urlRedirect')?>/bd"><img src="<?= base_url()?>assets/images/flags/ban.jpg"> Bangla</a></li>
                                    <li><a href="<?= site_url('admin/urlRedirect')?>/in"><img src="<?= base_url()?>assets/images/flags/ind.jpg"> India</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </nav>
            </header>
            <!-- Side-Nav-->
            <aside class="main-sidebar hidden-print">
                <section class="sidebar">
                    <div class="user-panel">
                        <div class="pull-left image"><img class="img-circle" src="<?= base_url(); ?>assets/images/users/<?= $this->session->userdata('current_user_image'); ?>" alt="User Image"></div>
                        <div class="pull-left info">
                            <h5><?php echo $this->session->userdata('current_user_fullName'); ?></h5>
                            <p class="designation"><?php echo $this->session->userdata('current_type'); ?></p>
                        </div>
                    </div>
                    <!-- Sidebar Menu-->
                    <ul class="sidebar-menu">
                        <li class="active"><a href=""><i class="fa fa-dashboard"></i><span>Dashboard</span></a></li>

                        <li class="treeview"><a href=""><i class="fa fa-laptop"></i><span>Customer</span><i class="fa fa-angle-right"></i></a>
                            <ul class="treeview-menu">
                                <li><a href="<?= base_url(); ?>ProfileView"><i class="fa fa-circle-o"></i> View</a></li>
                                <li><a href="<?= base_url(); ?>Customer/myBill"><i class="fa fa-circle-o"></i> My Bill</a></li>                
                            </ul>
                        </li> 
                        <li class="treeview"><a href=""><i class="fa fa-laptop"></i><span>Owner</span><i class="fa fa-angle-right"></i></a>
                            <ul class="treeview-menu">
                                <li><a href="<?= base_url(); ?>Owner/property"><i class="fa fa-circle-o"></i> Property</a></li>                
                                <li><a href="<?= base_url(); ?>Customer/addNew"><i class="fa fa-circle-o"></i> Add New</a></li>              
                            </ul>
                        </li>
                        <li class="treeview"><a href=""><i class="fa fa-laptop"></i><span>Admin</span><i class="fa fa-angle-right"></i></a>
                            <ul class="treeview-menu">
                                <li><a href="<?= base_url(); ?>Admin"><i class="fa fa-circle-o"></i>All Users</a></li>                
                            </ul>
                        </li>
                        <li class="treeview"><a href=""><i class="fa fa-laptop"></i><span>Setting</span><i class="fa fa-angle-right"></i></a>
                            <ul class="treeview-menu">
                                <li><a href="<?= base_url(); ?>Owner/addNewProperty"><i class="fa fa-circle-o"></i> Add Property</a></li>
                                <li><a href="<?= base_url(); ?>Owner/addNewFloor"><i class="fa fa-circle-o"></i> Add Floor Info</a></li>                
                                <li><a href=""><i class="fa fa-circle-o"></i> Add Note</a></li>   
                            </ul>
                        </li>
                        <li class="treeview"><a href="<?= base_url(); ?>Customising/LogoChange"><i class="fa fa-laptop"></i><span>Logo Change</span><i class="fa fa-angle-right"></i></a>
                            
                        </li>

                    </ul>
                </section>
            </aside>
            <div class="content-wrapper">
                <div class="page-title">
                    <div>
                        <h1><i class="fa fa-dashboard"></i> Dashboard</h1>
                        <p>A free and modular admin template</p>
                    </div>
                    <div>
                        <ul class="breadcrumb">
                            <li><i class="fa fa-home fa-lg"></i></li>
                            <li><a href="#">Dashboard</a></li>
                        </ul>
                    </div>
                </div>


                <!-- Main container -->

                <?php
                if (isset($home)) {
                    $this->load->view('dashboard/home');
                } elseif (isset($property)) {
                    $this->load->view('dashboard/property');
                } elseif (isset($property1)) {
                    $this->load->view('dashboard/property1');
                } elseif (isset($transection_page)) {
                    $this->load->view('dashboard/transection_page');
                } elseif (isset($addNew)) {
                    $this->load->view('dashboard/addNew_page');
                } elseif (isset($profileview_page)) {
                    $this->load->view('dashboard/profileview_page');
                } elseif (isset($myBill)) {
                    $this->load->view('dashboard/myBill');
                } elseif (isset($addNewProperty)) {
                    $this->load->view('dashboard/addNewProperty');
                } elseif (isset($addNewFloor)) {
                    $this->load->view('dashboard/addNewFloor');
                } elseif (isset($PropertyView_Page)) {
                    $this->load->view('dashboard/PropertyView_Page');
                } elseif (isset($ViewUserHistory_page)) {
                    $this->load->view('dashboard/ViewUserHistory_page');
                }  elseif (isset ($editInfor)) {
                    $this->load->view('dashboard/edit_page');
                }  elseif(isset ($admin_page)){
                    $this->load->view('dashboard/admin_page');
                } elseif (isset ($LogoChange)) {
                    $this->load->view('dashboard/LogoChange');
                }
                ?>

                <!-- Main container end-->

            </div>
        </div>
        <!-- Javascripts-->
        <script type="text/javascript" src="<?= base_url();?>assets/js/plugins/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="<?= base_url();?>assets/js/plugins/dataTables.bootstrap.min.js"></script>
        
        <script type="text/javascript" src="https://code.jquery.com/jquery-1.12.4.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.4.2/js/dataTables.buttons.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.4.2/js/buttons.print.min.js"></script>
    
        <script type="text/javascript">
                $('#sampleTable').DataTable( {
                    dom: 'Bfrtip',
                    buttons: [
                        'print'
                    ]
                } );
          
            
        </script>
        <script>
            $(document).on("keyup", ".qty1", function () {
                var sum = 0;
                $(".qty1").each(function () {
                    sum += +$(this).val();
                });
                $(".total").val(sum);
            });
            
            $(document).ready(function () {
                var sum = 0;
                $(".qty1").each(function () {
                    sum += +$(this).val();
                });
                $(".total").val(sum);
            });
        </script>
        <script src="<?= base_url(); ?>assets/js/jquery-2.1.4.min.js"></script>
        <script src="<?= base_url(); ?>assets/js/bootstrap.min.js"></script>
        <script src="<?= base_url(); ?>assets/js/plugins/pace.min.js"></script>
        <script src="<?= base_url(); ?>assets/js/main.js"></script>
        <script src="<?= base_url(); ?>assets/js/custom.js"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        
        <script>
            $(function () {
                $("#datepicker").datepicker({
                 dateFormat: 'yy-mm-dd',
                    changeYear: true,
                    changeMonth: true
                });
            });
        </script> 
        
    </body>
</html>