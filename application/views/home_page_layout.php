<!DOCTYPE html>
<html lang="en">
    <head>
        <title>
            <?php
                if(isset($site_home)){
                    echo "Home";
                }elseif($services){
                    echo "Services";
                }
            ?>
        </title>
        <meta charset="utf-8">
        <meta name="format-detection" content="telephone=no">
        <link rel="icon" href="<?= base_url(); ?>assets/images/favicon.ico" type="image/x-icon">
        <link rel="stylesheet" href="<?= base_url(); ?>assets/css/grid.css">
        <link rel="stylesheet" href="<?= base_url(); ?>assets/css/style2.css">
        <link rel="stylesheet" href="<?= base_url(); ?>assets/css/camera.css">
        <link rel="stylesheet" href="<?= base_url(); ?>assets/css/owl-carousel.css">
        <script src="<?= base_url(); ?>assets/js/jquery.js"></script>
        <script src="<?= base_url(); ?>assets/js/jquery.cookie.js"></script>
        <script src="<?= base_url(); ?>assets/js/jquery.easing.1.3.js"></script>
        <script src="<?= base_url(); ?>assets/js/jquery-migrate-1.2.1.js"></script>
        <script src="<?= base_url(); ?>assets/js/device.min.js"></script>
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,700' rel='stylesheet' type='text/css'>
        <!--before and after effect-->
        <!-- CSS reset -->
        <link rel="stylesheet" href="<?= base_url(); ?>assets/css/style123.css">
	<script src="<?= base_url(); ?>assets/js/modernizr.js"></script> <!-- Modernizr -->
        <!--before and after effect end-->
        
        <script type="text/javascript">
            $(document).ready(function (){
                console.log('bg-------test');
                $('.cd-handle').css('background', '#57aacd url("<?= base_url()?>assets/images/cd-arrows.svg") no-repeat center center');
            });
        </script>
        
    </head>
    <body>
        <div class="page">
            <!--
            ========================================================
                                                              HEADER
            ========================================================
            
            
            -->
            <header>
                <div class="container">
                    <div class="brand">
                        <h1 class="brand_name"><a href="<?= base_url('Home') ?>"><?= $name; ?></a></h1>
                        <p class="brand_slogan">Company</p>
                    </div><a href="callto:#" class="fa-phone">800-2345-6789</a>
                    <p>One of our representatives will happily contact you within 24 hours. For urgent needs call us at</p>
                </div>
                <div id="stuck_container" class="stuck_container">
                    <div class="container">
                        <nav class="nav">
                            <ul id="mainMenu" data-type="navbar" class="sf-menu">
                                <li class="<?= isset($site_home) ? 'active' : NULL;?>"><a href="<?= base_url('Home') ?>">Home</a>
                                </li>
                                <li><a href="">About</a>
                                    <ul>
                                        <li><a href="#">Lorem ipsum dolor</a></li>
                                        <li><a href="#">Conse ctetur adipisicing</a></li>
                                        <li><a href="#">Elit sed do eiusmod
                                                <ul>
                                                    <li><a href="#">Lorem ipsum</a></li>
                                                    <li><a href="#">Conse adipisicing</a></li>
                                                    <li><a href="#">Sit amet dolore</a></li>
                                                </ul></a></li>
                                        <li><a href="#">Incididunt ut labore</a></li>
                                        <li><a href="#">Et dolore magna</a></li>
                                        <li><a href="#">Ut enim ad minim</a></li>
                                    </ul>
                                </li>
                                <li class="<?= isset($services) ? 'active' : NULL;?>"><a href="<?= base_url('Services') ?>">Services</a>
                                </li>
                                <li><a href="#">FAQS</a>
                                </li>
                                <li><a href="#">Contacts</a>
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </header>
            <!--
            ========================================================
                                        CONTENT
            ========================================================
            -->


            <?php
            if (isset($site_home)) {
                $this->load->view('site/home');
            } elseif (isset($services)) {
                $this->load->view('site/services');
            }
            ?>


            <!--
            ========================================================
                                        FOOTER
            ========================================================
            -->
            <footer>
                <section class="well3">
                    <div class="container">
                        <ul class="row contact-list">
                            <li class="grid_4">
                                <div class="box">
                                    <div class="box_aside">
                                        <div class="icon2 fa-map-marker"></div>
                                    </div>
                                    <div class="box_cnt__no-flow">
                                        <address>4578 Marmora Road,Glasgow<br/> D04 89GR</address>
                                    </div>
                                </div>
                                <div class="box">
                                    <div class="box_aside">
                                        <div class="icon2 fa-envelope"></div>
                                    </div>
                                    <div class="box_cnt__no-flow"><a href="mailto:#">info@demolink.org</a></div>
                                </div>
                            </li>
                            <li class="grid_4">
                                <div class="box">
                                    <div class="box_aside">
                                        <div class="icon2 fa-phone"></div>
                                    </div>
                                    <div class="box_cnt__no-flow"><a href="callto:#">800-2345-6789</a></div>
                                </div>
                                <div class="box">
                                    <div class="box_aside">
                                        <div class="icon2 fa-fax"></div>
                                    </div>
                                    <div class="box_cnt__no-flow"><a href="callto:#">800-2345-6790</a></div>
                                </div>
                            </li>
                            <li class="grid_4">
                                <div class="box">
                                    <div class="box_aside">
                                        <div class="icon2 fa-facebook"></div>
                                    </div>
                                    <div class="box_cnt__no-flow"><a href="#">Follow on facebook</a></div>
                                </div>
                                <div class="box">
                                    <div class="box_aside">
                                        <div class="icon2 fa-twitter"></div>
                                    </div>
                                    <div class="box_cnt__no-flow"><a href="#">Follow on Twitter</a></div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </section>
                <section>
                    <div class="container">
                        <div class="copyright">Business Company © <span id="copyright-year"></span>.&nbsp;&nbsp;<a href="index-5.html">Privacy Policy</a><!-- {%FOOTER_LINK} -->
                        </div>
                    </div>
                </section>
            </footer>
        </div>
        <script src="<?= base_url(); ?>assets/js/script.js"></script>
        <script src="<?= base_url(); ?>assets/js/jquery.mobile.custom.min.js"></script> <!-- Resource jQuery -->
        <script src="<?= base_url(); ?>assets/js/main.js"></script> <!-- Resource jQuery -->
    </body>
</html>