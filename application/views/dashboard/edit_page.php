<style type="text/css">
    .hideme{
        display: none;
    
    }
</style>


<form class="form-horizontal" method="POST" action="" enctype="multipart/form-data">
<div class="col-lg-4">
    <div class="well bs-component">
            <fieldset>
                <legend>Basic Info</legend>
                <div class="form-group">
                    <label class="col-lg-4 control-label">Name</label>
                    <div class="col-lg-8">
                        <input name="name" class="form-control" type="text" value="<?= ($editInfoData ? $editInfoData->full_name : '');?>" placeholder="Type your name...">
                        <span class="text-danger"><?= form_error('name');?></span>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-lg-4 control-label" for="">Type</label>
                    <div class="col-lg-8">
                        <select name="type" class="form-control" id="select">   
                            <option <?= ($editInfoData ? $editInfoData->type : 'selected="selected"');?>value="0">Select</option>
                            <option <?php if($editInfoData){ if($editInfoData->type == 'Admin'){echo 'selected="selected"';}else{NULL;}}else{NULL;} ?>value="Admin">Admin</option>
                            <option <?php if($editInfoData){ if($editInfoData->type == 'Owner'){echo 'selected="selected"';}else{NULL;}}else{NULL;} ?>value="Owner">Owner</option>
                            <option <?php if($editInfoData){ if($editInfoData->type == 'Customer'){echo 'selected="selected"';}else{NULL;}}else{NULL;} ?>value="Customer">Customer</option>
                            
                            
                        </select> 
                        <span class="text-danger"><?= form_error('type');?></span>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-4 control-label" >Phone no</label>
                    <div class="col-lg-8">
                        <input name="phone" class="form-control" type="text" placeholder="" value="<?= $editInfoData->phone;?>">
                        <span class="text-danger"><?= form_error('phone');?></span>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-lg-4 control-label" >Email</label>
                    <div class="col-lg-8">
                        <input name="email" class="form-control" type="text" placeholder="" value="<?= $editInfoData->email;?>">
                        <span class="text-danger"><?= form_error('email');?></span>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-4 control-label">Username</label>
                    <div class="col-lg-8">
                        <input name="username" class="form-control" type="text" placeholder="" value="<?= $editInfoData->username;?>">
                        <span class="text-danger"><?= form_error('username');?></span>
                    </div>
                </div>
                <!--<div class="form-group">
                    <label class="col-lg-4 control-label">Password</label>
                    <div class="col-lg-8">
                        <input name="password" class="form-control" type="password" placeholder="" value="<?= $editInfoData->password;?>">
                        <span class="text-danger"><?= form_error('password');?></span>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-4 control-label">Confirm Password</label>
                    <div class="col-lg-8">
                        <input name="c_password" class="form-control" type="password" placeholder="" value="<?= $editInfoData->password;?>">
                        <span class="text-danger"><?= form_error('c_password');?></span>
                    </div>
                </div>-->
                <div class="form-group">
                    <label class="col-lg-4 control-label">User/Admin/Owner image</label>
                    <div class="col-lg-8">
                        <input name="image" class="form-control"  type="file" placeholder="image" value="Michael-jackson6.jpg">
                        <span class="text-danger"><?= form_error('image');?></span>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-4 control-label">National ID <br><span class="text-primary">jpg.jpeg,png</span></label>
                    <div class="col-lg-8">
                        <input name="national_id" class="form-control"  type="file" placeholder="image" value="<?= $editInfoData->national_id;?>">
                        <span class="text-danger"><?= form_error('national_id');?></span>
                    </div>
                </div>
            </fieldset>
    </div>
</div>

<div class="col-lg-4">
    <div class="well bs-component">        
        <div class="form-horizontal">
            <legend>Details</legend>
            <fieldset>
                <div class="form-group">
                    <label class="col-lg-3 control-label" for="textArea">Select Bulding</label>
                    <div class="col-lg-9">
                        <select name="buiding" class="form-control buildingClass" id="select">
                            <?php
                                foreach ($budingInformation as $buidingData){
                            ?>
                                <option <?= $buidingData->id == $editInfoData->buiding ? 'selected="selected"' : null?> class=''><?= $buidingData->property;?></option>
                            <?php
                                }
                            ?>
                        </select>
                        <span class="text-danger"><?= form_error('buiding');?></span>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-3 control-label" for="inputPassword">Floor no</label>
                    <div class="col-lg-9">
                        <select name="floor_name" class="form-control floorlist" id="">
                            <option value="0" class="">Select</option>
                            <?php
                                //foreach ($floorInforMation as $floorData){
                            ?>
                            <option  selected="selected"><?= $editInfoData->floor_name ?></option>
                            <?php
                                //}
                            ?>
                        </select>  
                        <span class="text-danger"><?= form_error('floor_name');?></span>
                    </div>
                </div>
            
                <div class="form-group">
                    <label class="col-lg-3 control-label">Status</label>
                    <div class="col-lg-9">
                        <div class="radio">
                            <label>
                                <?php
                                    if($editInfoData->status == 1){
                                        echo "<input type='radio' name='status' value='1' checked>Active <br><br> <input type='radio' name='status' value='0'>Inactive";
                                    }else{
                                        echo "<input type='radio' name='status' value='1'>Active <br><br> <input type='radio' name='status' value='0' checked>Inactive";
                                    }
                                ?>
                            </label>       
                            <span class="text-danger"><?= form_error('status');?></span>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-3 control-label" >Start Date</label>
                    <div class="col-lg-9">
                        <input name="start_date" class="form-control" id="datepicker" type="text" placeholder="" value="<?= $editInfoData->start_date ?>">
                        <span class="text-danger"><?= form_error('start_date');?></span>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-3 control-label" for="">Total Members</label>
                    <div class="col-lg-9">
                        <input name="total_member" class="form-control" id="" type="text" placeholder="" value="<?= $editInfoData->total_member ?>">
                        <span class="text-danger"><?= form_error('total_member');?></span>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-3 control-label" for="">Members Details</label>
                    <div class="col-lg-9">
                        <textarea name="member_details" class="form-control" rows="3"><?= $editInfoData->member_details ?></textarea>
                        <span class="text-danger"><?= form_error('member_details');?></span>
                    </div>
                </div>
            </fieldset>
        </div>
    </div>
</div>
<div class="col-lg-4">
    <div class="well bs-component">
        <div class="form-horizontal">
            <legend>Payment Info</legend>
            <fieldset>
                <div class="form-group">
                    <label class="col-lg-4 control-label" for="">Advance House Rent</label>
                    <div class="col-lg-8">
                        <input name="advance_payment" class="form-control" type="text" placeholder="" value="<?= $editInfoData->advance_payment ?>">
                        <span class="text-danger"><?= form_error('advance_payment');?></span>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-4 control-label" for="">Advance Electricity Bill</label>
                    <div class="col-lg-8">
                        <input name="advance_electricity_bill" class="form-control" type="text" placeholder="" value="<?= $editInfoData->advance_electricity_bill ?>">
                        <span class="text-danger"><?= form_error('advance_electricity_bill');?></span>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-4 control-label" for="">House Rent Per Month</label>
                    <div class="col-lg-8">
                        <input name="house_rent_per_month" class="form-control" type="text" placeholder="" value="<?= $editInfoData->house_rent_per_month ?>">
                        <span class="text-danger"><?= form_error('house_rent_per_month');?></span>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-4 control-label" for="">Electricity Bill</label>
                    <div class="col-lg-8">
                        <input name="electricity_bill_per_month" class="form-control" type="text" placeholder="" value="<?= $editInfoData->electricity_bill_per_month ?>">
                        <span class="text-danger"><?= form_error('electricity_bill_per_month');?></span>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-4 control-label" for="">Gass Bill</label>
                    <div class="col-lg-8">
                        <input name="gass_bill_per_month" class="form-control" type="text" placeholder="" value="<?= $editInfoData->gass_bill_per_month ?>">
                        <span class="text-danger"><?= form_error('gass_bill_per_month');?></span>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-4 control-label" for="">Water Bill</label>
                    <div class="col-lg-8">
                        <input name="water_bill_per_month" class="form-control" type="text" placeholder="" value="<?= $editInfoData->water_bill_per_month ?>">
                        <span class="text-danger"><?= form_error('water_bill_per_month');?></span>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-4 control-label" for="">Others Payment</label>
                    <div class="col-lg-8">
                        <input name="other_payment" value="0" class="form-control" type="text" placeholder="" value="<?= $editInfoData->other_payment ?>">  
                        <span class="text-danger"><?= form_error('other_payment');?></span>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-4 control-label" for=""></label>
                    <div class="col-lg-8">
                        <button class="btn btn-default" type="reset">Cancel</button>
                        <button class="btn btn-primary" type="submit">Submit</button>
                    </div>
                </div>
            </fieldset>
        </div>
    </div>
</div>
</form>