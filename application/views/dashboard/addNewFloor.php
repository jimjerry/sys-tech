<div class="row">
    <div class="col-md-6">
        <div class="card">
            <h3 class="card-title">Add Floor</h3>
            <form class="form-horizontal" method="POST" action="">
                <div class="card-body">                  
                    <div class="form-group">
                        <label class="control-label col-md-3">Floor name</label>
                        <div class="col-md-8">
                            <input name="floorname" class="form-control" type="text" placeholder="">
                            <span class="text-danger"><?= form_error('floorname'); ?></span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3">Select Building</label>
                        <div class="col-md-8">
                            <select name="budiling" class="form-control">
                                <?php
                                foreach ($PropertData as $dataOfProperty) {
                                    echo "<option value='$dataOfProperty->id'>" . $dataOfProperty->property . "</option>";
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <input type="hidden" name="status" value="0">
                </div>
                <div class="card-footer">
                    <div class="row">
                        <div class="col-md-8 col-md-offset-3">
                            <button class="btn btn-primary icon-btn" type="submit"><i class="fa fa-fw fa-lg fa-check-circle"></i>Add</button>&nbsp;&nbsp;&nbsp;<a class="btn btn-default icon-btn" href="#"><i class="fa fa-fw fa-lg fa-times-circle"></i>Cancel</a>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <!--<div class="col-md-6">
        <div class="">
            <h3 class="card-title">All Properties</h3>
            <?php
            foreach ($PropertData as $dataOfProperty) {
                ?>
                <div class="widget-small primary coloured-icon"><i class="icon fa fa-users fa-3x"></i>
                    <div class="info">
                        <div class="col-md-6">
                            <h4><?= $dataOfProperty->property; ?></h4>
                            <p><b>Total Floor: <?= $dataOfProperty->total; ?> <br>Customer Avaialbe: 5 <br> Free Floor: 0</b></p>
                        </div>
                        <div class="col-md-6">
                            <button class="btn-sm btn-primary">View</button>                            
                            <button class="btn-sm btn-warning">Edit</button>
                            <button class="btn-sm btn-danger">Delete</button>
                        </div>
                    </div>
                </div>
                <?php
            }
            ?>
        </div>
    </div>-->
</div>