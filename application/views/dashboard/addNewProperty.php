<div class="row">
    <div class="col-md-6">
            <div class="card">
              <h3 class="card-title">Add Property</h3>
              <form class="form-horizontal" method="POST" action="">
              <div class="card-body">                  
                  <div class="form-group">
                    <label class="control-label col-md-3">Property name</label>
                    <div class="col-md-8">
                        <input name="propertyName" class="form-control" type="text" placeholder="">
                    </div>
                  </div>
                  
                  <div class="form-group">
                    <label class="control-label col-md-3">Address</label>
                    <div class="col-md-8">
                        <textarea name="addressOfProperty" class="form-control" rows="4" placeholder=""></textarea>
                    </div>
                  </div>                  
                
              </div>
              <div class="card-footer">
                <div class="row">
                  <div class="col-md-8 col-md-offset-3">
                      <button class="btn btn-primary icon-btn" type="submit"><i class="fa fa-fw fa-lg fa-check-circle"></i>Add</button>&nbsp;&nbsp;&nbsp;<a class="btn btn-default icon-btn" href="#"><i class="fa fa-fw fa-lg fa-times-circle"></i>Cancel</a>
                  </div>
                </div>
              </div>
              </form>
            </div>
          </div>
</div>