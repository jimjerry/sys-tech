<div class="row">
    <div class="col-md-12">
        <div class="card">
            <h3 class="card-title">
                <span class="text-primary">
                    All Users
                </span>
            </h3>
            <div class="card-body table-responsive">
                <table class="table">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>User Name</th>
                    <th>Type</th>
                    <th>Full Name</th>
                    <th>Email</th>
                    <th>Phone</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                    <?php
                        $x =1;                        
                        foreach ($allUsers as $data){
                    ?>
                  <tr class="info">
                    <td><?= $x;?></td>
                    <td><?= $data->username;?></td>
                    <td><?= $data->type;?></td>
                    <td><?= $data->full_name;?></td>
                    <td><?= $data->email;?></td>
                    <td><?= $data->phone;?></td>
                 
                    
                    <td>
                        <a href=""><button class="btn-sm btn-primary">View</button></a>
                        <a href="<?= base_url();?>Customer/editInfo?id=<?= $data->id?>"><button class="btn-sm btn-warning">Edit</button></a>
                        <button id="" class="btn-sm btn-danger">Delete</button>
                    </td>
                  </tr>
                    <?php
                        $x++;               
                          }
                    ?>
                </tbody>
              </table>
            </div>
        </div>
    </div>
</div>