<!DOCTYPE html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css">
        <style type="text/css">
            body{
                background: #eaeae9;
                // height: 1613px;
            }
            .container{
                // background: #fff;
            }
            p{
                padding: 0;
                margin: 0;
            }
            h1{
                font-size: 25px;
            }
            h2{
                font-size: 22px;
                line-height: 22px;
            }
            h3{
                font-size: 20px;
            }
            h4{
                font-size: 18px;
            }
            .borderBottom{
                border-bottom: 2px solid #d4d4d4;
            }
            .signBorder{
                border-bottom: 1px solid #000;
            }
            .mainDIV{
                background: #fff;
                width: 1000px;
                height: 1100px;
                margin: 0 auto;
            }
        </style>
    </head>
    <body>

        <div class="mainDIV">
            <header style="padding: 20px 0;">
                <div class="container text-center">
                    <div class="row">
                        <div class="col-lg-12">
                            <h1>House name</h1>
                            <p>D.C.C. 300/1 South Kafrul, Dhaka - Cantonment, Dhaka - 1206</p>
                            <p>Contact: 0191315155, 0225964598798</p>
                        </div>
                    </div>
                </div>
            </header>
            <section>
                <div class="container borderBottom"></div>
            </section>

            <section>
                <div class="container" style="padding-top: 50px;">
                    <div class="row">
                        <div class="col-lg-12 text-center">
                            <h2>Month of October, 2017</h2>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4"></div>
                        <div class="col-sm-6">
                            <table class="table table-hover">
                                <thead>
                                    <tr>
                                        <th class="text-right">Details</th>
                                        <td></td>
                                        <th>Amount</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td class="text-right">House Rent</td>
                                        <td>=</td>
                                        <td>15000/=</td>
                                    </tr>
                                    <tr>
                                        <td class="text-right">Electricity Bill</td>
                                        <td>=</td>
                                        <td>800/= (Month of September, 2017)</td>
                                    </tr>
                                    <tr>
                                        <td class="text-right">Gass Bill</td>
                                        <td>=</td>
                                        <td>800/=</td>
                                    </tr>
                                    <tr>
                                        <td class="text-right">Water Bill</td>
                                        <td>=</td>
                                        <td>600/=</td>
                                    </tr>
                                    <tr>
                                        <td class="text-right">Others</td>
                                        <td>=</td>
                                        <td>0.00/=</td>
                                    </tr>
                                    <tr>
                                        <td class="text-right"><b>Total</b></td>
                                        <td>=</td>
                                        <td><b>15000/=</b></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="col-lg-1">

                        </div>
                    </div>
                </div>
            </section>

            <section style="padding-top: 210px;">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-6 text-center">
                            <span class="signBorder">Signature of Owner</span>
                        </div>
                        <div class="col-lg-6 text-center">
                            <span class="signBorder">Signature of Rented</span>
                        </div>
                    </div>
                </div>
            </section>
           
            <footer style="padding: 20px 0; margin-top: 185px; border-top: 1px solid #d4d4d4;" class="fix-bottom">
                <div class="container text-center">
                    <div class="row">
                        <div class="col-lg-12">
                            <p>D.C.C. 300/1 South Kafrul, Dhaka - Cantonment, Dhaka - 1206</p>
                        </div>
                    </div>
                </div>
            </footer>
        </div>

        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js"></script>
    </body>
</html>