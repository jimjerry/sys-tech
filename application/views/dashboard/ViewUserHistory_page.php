<style type="text/css">
    .minHeight{
        min-height: 500px;
        //height: auto;
    }
</style>
<div class="row user">
    <div class="col-md-12">
        <div class="profile">
            <div class="info"><img class="user-img" src="<?= base_url(); ?>assets/images/users/<?= $ViewUserHistoryData->image; ?>">
                <h4><?= $ViewUserHistoryData->full_name; ?></h4>
                <p><?= $ViewUserHistoryData->type; ?></p>
            </div>
            <div class="cover-image"></div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="card p-0">
            <ul class="nav nav-tabs nav-stacked user-tabs">
                <li class=""><a href="#user-timeline" data-toggle="tab" aria-expanded="">Timeline</a></li>
                <li class=""><a href="#user-settings" data-toggle="tab" aria-expanded="">Settings</a></li>
                <li class=""><a href="#make-transaction" data-toggle="tab" aria-expanded="">Make Bill</a></li>
                <li class=""><a href="#payment-history" data-toggle="tab" aria-expanded="">Payment History</a></li>
            </ul>
        </div>
    </div>
    <div class="col-md-9">
        <div class="tab-content">
            <div class="tab-pane active" id="user-timeline">
                <div class="timeline">
                    <div class="post">
                        <h5> 
                            <?php
                            if ($monthData) {
                                echo "Month of " . $month = date("F") . " is " . "<span style='color: green'>Paid</span>" . " ( " . $monthData->date . ")";
                            } else {

                                echo "Month of " . $month = date("F") . " is " . "<span style='color: red'>Not Paid</span>";
                            }
                            ?>
                        </h5>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="table-responsive">
                                    <table class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <th>Bills Details</th>
                                                <th>Amounts</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>House Rent</td>
                                                <td><?= $ViewUserHistoryData->house_rent_per_month ?></td>
                                            </tr>
                                            <tr>
                                                <td>Electricity Bill</td>
                                                <td><?= $ViewUserHistoryData->electricity_bill_per_month ?> ( <span class="text-warning">Month of September</span> )</td>
                                            </tr>
                                            <tr>
                                                <td>Gass Bill</td>
                                                <td><?= $ViewUserHistoryData->gass_bill_per_month ?></td>
                                            </tr>
                                            <tr>
                                                <td>Water Bill</td>
                                                <td><?= $ViewUserHistoryData->water_bill_per_month ?></td>
                                            </tr>
                                            <tr>
                                                <td><b>Total</b></td>
                                                <td><b><input type="text" name="totalAmount" class="total" value="" readonly></b></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <p>Advance Rent: <b><span class="text-success"><?= $ViewUserHistoryData->advance_payment;?> tk</span></b></p>
                                <p>Advance Electricity: <b><span class="text-danger"><?= $ViewUserHistoryData->advance_electricity_bill;?></span></b> tk</p>
                                <img class="img-responsive" src="<?= base_url();?>assets/images/users/<?= $ViewUserHistoryData->national_id;?>">
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <div class="tab-pane fade in" id="user-settings">
                <div class="card user-settings minHeight">
                    <h4 class="line-head">Bill Settings</h4>
                    <div class="col-md-6">
                        <form action="<?= base_url(); ?>Owner/payableBill/?id=<?= $ViewUserHistoryData->id ?>" method="POST">
                            <div class="row">
                                <div class="col-md-6 col-sm-4 mb-20">
                                    <label>House Rent</label>
                                    <input name="house_rent_per_month" id="house_rent_per_month" class="form-control" type="text" value="<?= $ViewUserHistoryData->house_rent_per_month ?>">
                                </div>

                                <div class="col-md-6 col-sm-4 mb-20">
                                    <label>Electricity Bill</label>
                                    <input name="electricity_bill_per_month" id="electricity_bill_per_month" class="form-control" type="text" value="<?= $ViewUserHistoryData->electricity_bill_per_month ?>">
                                </div>                  
                                <div class="col-md-6 col-sm-4 mb-20">
                                    <label>Gass Bill</label>
                                    <input name="gass_bill_per_month" id="gass_bill_per_month" class="form-control" type="text" value="<?= $ViewUserHistoryData->gass_bill_per_month ?>">
                                </div>
                                <div class="col-md-6 col-sm-4 mb-20">
                                    <label>Water Bill</label>
                                    <input name="water_bill_per_month" class="form-control" type="text" value="<?= $ViewUserHistoryData->water_bill_per_month ?>">
                                </div>

                            </div>

                            <div class="row mb-10">
                                <div class="col-md-12">
                                    <button class="btn btn-primary" type="submit"><i class="fa fa-fw fa-lg fa-check-circle"></i> Save</button>
                                    <a onclick="myFunction()"><button class="btn btn-primary" type="button"><i class="fa fa-file-pdf-o fa-sm fa-check-circle"></i> Make PDF</button></a>
                                    <button class="btn btn-primary" type="submit"><i class="fa fa-paper-plane fa-sm fa-check-circle"></i> Send Mail</button>
                                </div>
                            </div>

                            <script type="text/javascript">

                                function myFunction()
                                {
                                    var house = $("#house_rent_per_month").val();
                                    var elc_bil = $("#electricity_bill_per_month").val();
                                    var gass_bil = $("#gass_bill_per_month").val();
                                    var url = '<?= base_url(); ?>';
                                    window.open(url + 'Pdf_maker?house=' + house + '&elc_bil=' + elc_bil + '&gass_bil=' + gass_bil, '_blank');

                                }

                            </script>

                        </form>
                    </div>
                    <div class="col-md-6"></div>
                </div>
            </div>
            <div class="tab-pane fade in" id="make-transaction">
                <div class="card user-settings">
                    <h4 class="line-head">Make Bill</h4>
                    <div class="row">
                        <div class="col-sm-6 col-xs-12">
                            <form class="form-horizontal" action="" method="POST">
                                <div class="form-group">
                                    <label class="control-label col-md-3">Hourse Rent</label>
                                    <div class="col-md-8">
                                        <input class="form-control qty1" type="text" name="houseRent" placeholder="" value="<?= $ViewUserHistoryData->house_rent_per_month ?>">
                                        <span class="text-danger"><?= form_error('houseRent'); ?></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Electric Bill</label>
                                    <div class="col-md-8">
                                        <input class="form-control qty1" name="electricBill" type="text" placeholder="" value="<?= $ViewUserHistoryData->electricity_bill_per_month ?>">
                                        <span class="text-danger"><?= form_error('electricBill'); ?></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Gass Bill</label>
                                    <div class="col-md-8">
                                        <input class="form-control qty1" name="gassBill" type="text" placeholder="" value="<?= $ViewUserHistoryData->gass_bill_per_month ?>">
                                        <span class="text-danger"><?= form_error('gassBill'); ?></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Water Bill</label>
                                    <div class="col-md-8">
                                        <input class="form-control qty1" name="waterBill" type="text" placeholder="" value="<?= $ViewUserHistoryData->water_bill_per_month ?>">
                                        <span class="text-danger"><?= form_error('waterBill'); ?></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Others</label>
                                    <div class="col-md-8">
                                        <input class="form-control qty1" name="otherBill" type="text" value="0" placeholder="">
                                        <span class="text-danger"><?= form_error('otherBill'); ?></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Date</label>
                                    <div class="col-md-8">
                                        <input name="start_date" class="form-control" id="datepicker" type="text" placeholder="" value="">
                                        <span class="text-danger"><?= form_error('start_date'); ?></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Total</label>
                                    <div class="col-md-8">
                                        <input type="text" name="totalAmount" class="form-control total" value="">
                                        <span class="text-danger"><?= form_error('totalAmount'); ?></span>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3"></label>
                                    <div class="col-md-8">
                                        <button type="submit" class="btn btn-primary">Done</button>
                                    </div>
                                </div>


                            </form>
                        </div>
                        <div class="col-sm-6 col-xs-6"></div>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade in" id="payment-history">
                <div class="card user-settings">
                    <h4 class="line-head">Payment History</h4>

                    <!paymeny history>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="">
                                <div class="card">
                                    <div id="" class="card-body table-responsive">

                                        <table class="table table-hover table-bordered" id="sampleTable">
                                            <thead>
                                                <tr>
                                                    <th>House rent</th>
                                                    <th>Electricity bill</th>
                                                    <th>Gass Bill</th>
                                                    <th>Water Bill</th>
                                                    <th>Others Bill</th>
                                                    <th>Total Amount</th>
                                                    <th>Date</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                foreach ($ViewUserHistoryData2 as $dataHistory) {
                                                    ?>

                                                    <tr role="row" class="odd">
                                                        <td><?= $dataHistory->house_rent_per_month ?></td>
                                                        <td><?= $dataHistory->electricity_bill_per_month ?></td>
                                                        <td><?= $dataHistory->gass_bill_per_month ?></td>
                                                        <td><?= $dataHistory->water_bill_per_month ?></td>
                                                        <td><?= $dataHistory->other_payment ?></td>
                                                        <td><?= $dataHistory->total_amount ?></td>
                                                        <td><?= $dataHistory->date ?></td>
                                                    </tr>
                                                    <?php
                                                }
                                                ?>

                                            </tbody>
                                        </table>


                                        <!--<div class="row">
                                            <div class="col-sm-6">
                                                <div class="dataTables_length" id="sampleTable_length">
                                                    <label>Show <select name="sampleTable_length" aria-controls="sampleTable" class="form-control input-sm">
                                                            <option value="10">10</option>
                                                            <option value="25">25</option><option value="50">50</option>
                                                            <option value="100">100</option>
                                                        </select> entries</label>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div id="sampleTable_filter" class="dataTables_filter">
                                                    <label>Search:<input type="search" class="form-control input-sm" placeholder="" aria-controls="sampleTable"></label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-12 table-responsive">
                                                <table class="table table-hover table-bordered dataTable no-footer" id="sampleTable" role="grid" aria-describedby="sampleTable_info">
                                                    <thead>
                                                        <tr role="row">
                                                            <th>House rent</th>
                                                            <th>Electricity bill</th>
                                                            <th>Gass Bill</th>
                                                            <th>Water Bill</th>
                                                            <th>Others Bill</th>
                                                            <th>Total Amount</th>
                                                            <th>Date</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        
                                        <?php
                                        foreach ($ViewUserHistoryData2 as $dataHistory) {
                                            ?>
                                                                    
                                                                    <tr role="row" class="odd">
                                                                        <td><?= $dataHistory->house_rent_per_month ?></td>
                                                                        <td><?= $dataHistory->electricity_bill_per_month ?></td>
                                                                        <td><?= $dataHistory->gass_bill_per_month ?></td>
                                                                        <td><?= $dataHistory->water_bill_per_month ?></td>
                                                                        <td><?= $dataHistory->other_payment ?></td>
                                                                        <td><?= $dataHistory->total_amount ?></td>
                                                                        <td><?= $dataHistory->date ?></td>
                                                                    </tr>
                                            <?php
                                        }
                                        ?>
                                                        
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-5">
                                                <div class="dataTables_info" id="sampleTable_info" role="status" aria-live="polite">Showing 1 to 10 of 57 entries</div>
                                            </div>
                                            <div class="col-sm-7">
                                                <div class="dataTables_paginate paging_simple_numbers" id="sampleTable_paginate">
                                                    <ul class="pagination">
                                                        <li class="paginate_button previous disabled" id="sampleTable_previous">
                                                            <a href="#" aria-controls="sampleTable" data-dt-idx="0" tabindex="0">Previous</a>
                                                        </li>
                                                        <li class="paginate_button active"><a href="#" aria-controls="sampleTable" data-dt-idx="1" tabindex="0">1</a></li>
                                                        <li class="paginate_button "><a href="#" aria-controls="sampleTable" data-dt-idx="2" tabindex="0">2</a></li>
                                                        <li class="paginate_button "><a href="#" aria-controls="sampleTable" data-dt-idx="3" tabindex="0">3</a></li>
                                                        <li class="paginate_button "><a href="#" aria-controls="sampleTable" data-dt-idx="4" tabindex="0">4</a></li>
                                                        <li class="paginate_button "><a href="#" aria-controls="sampleTable" data-dt-idx="5" tabindex="0">5</a></li>
                                                        <li class="paginate_button "><a href="#" aria-controls="sampleTable" data-dt-idx="6" tabindex="0">6</a></li>
                                                        <li class="paginate_button next" id="sampleTable_next"><a href="#" aria-controls="sampleTable" data-dt-idx="7" tabindex="0">Next</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>-->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!paymeny history end>
                </div>
            </div>
        </div>
    </div>
</div>