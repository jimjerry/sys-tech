<div class="row">
<div class="col-md-12">
    <div class="card table-responsive">
        <h3 class="card-title">Property list</h3>
        <table class="table">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Name</th>
                    <th>Address</th>
                    <th>Total Floor</th>
                    <th>Active</th>
                    <th>Free Floor</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                <?php
                    $x =1;
                    foreach ($PropertData as $dataOfProperty) {
                ?>
                <tr class="info">
                    <td><?= $x;?></td>
                    <td><?=$dataOfProperty->property ?></td>
                    <td><?=$dataOfProperty->address ?></td>
                    <td><?=$dataOfProperty->total_floor ?></td>
                    <td><?=$dataOfProperty->total_active ?></td>
                    <td>
                        <?php
                            echo $dataOfProperty->total_floor-$dataOfProperty->total_active;
                        ?>
                    </td>
                    <td>
                        <a href="<?= base_url();?>Owner/propertyView?id=<?=$dataOfProperty->buidling_id ?>"><button class="btn-sm btn-primary">View</button></a>
                        <a href="<?= base_url();?>Login/editInfo?id=<?=$dataOfProperty->id ?>"><button class="btn-sm btn-warning">Edit</button></a>
                        <button class="btn-sm btn-danger">Delete</button>
                    </td>
                </tr> 
                
               <?php
               $x++;               
                 }
                ?>
            </tbody>
        </table>
    </div>
</div>
</div>