<div class="row">
    <div class="col-md-12">
        <div class="card">
            <h3 class="card-title">
                <span class="text-primary">
                    <?= $PropertyViewData2->property; ?>
                </span>
            </h3>
            <div class="card-body table-responsive">
                <table class="table">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>Client Name</th>
                    <th>Floor</th>
                    <th>Phone</th>
                    <th>Email</th>
                    <th><?php echo "Month of ".$month = date("F"); ?></th>
                    <th>Status</th>
                    <th>Image</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                    <?php
                        $x =1;
                        
                        
                        foreach ($PropertyViewData as $data){
                    ?>
                  <tr class="info">
                    <td><?= $x;?></td>
                    <td><?= $data->full_name;?></td>
                    <td><?= $data->floor_name?></td>
                    <td><?= $data->phone?></td>
                    <td><?= $data->email?></td>
                    <td>
                        <?php                        
                        if(!empty($paidArray))
                        {
                            if(in_array($data->id, $paidArray))
                            {                             
                                 echo "<span style='color: green'>Paid</span>";
                            }else{
                                
                                echo "<span style='color: red'>Not Paid</span>";
                            }        
                        }   
                            
                        ?>
                    </td>
                    <td>
                        <?php
                        if($data->status > 0){
                            echo "<b class='text-primary'>Active</b>";
                        }else{
                            echo "<b class='text-danger'>Inactive</b>";
                        }
                        ?>
                    </td>
                    <td><img id="userImage" src="<?= base_url();?>/assets/images/users/<?=$data->image?>"></td>
                    <td>
                        <a href="<?= base_url();?>Customer/ViewUserHistory?id=<?= $data->id?>"><button class="btn-sm btn-primary">View</button></a>
                        <a href="<?= base_url();?>Customer/editInfo?id=<?= $data->id?>"><button class="btn-sm btn-warning">Edit</button></a>
                        <button id="<?= $data->id;?>" class="btn-sm btn-danger ajaxDelete">Delete</button>
                    </td>
                  </tr>
                        <?php $x++; } ?>
                </tbody>
              </table>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    
        $(document).on("click", "button.ajaxDelete", function(){
            var url = '<?php echo base_url()?>';
            var id = $(this).attr('id');    
             
            var rows=$(this).parents('tr') ;
            
            var result = confirm("Want to delete?");
            if (result){
                $.post(url+'Login/delete',{delid:id},function(view){
 
                    if(view=="done")
                    {
                      rows.fadeOut();
                    }
                    else{
                        alert("Some this is wrong ! may be some table data is not delete ! Please see console for details");
                        console.log(view);
                        
                    }
                });
            } 
        });

</script>